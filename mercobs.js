// -----------------------------------------------------------------------------
// File       : mercobs.js
// Purpose    : an ephemeris for Mercury observation
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Created on : 2021-MAY-14
// Modified on: 2021-MAY-21
// Reference  : Peter Duffett-Smith, Practical Astronomy with your Calculator,
//              3rd ed., Cambridge University Press, 1995.
// -----------------------------------------------------------------------------

// A small GeolocationHelper object.
// Constructor takes as argument the input widgets for
// geographical coordinates
function GeolocationHelper(lat_deg, lat_min, lat_sec, lon_deg, lon_min, lon_sec, site_select)
{
   this.lat_deg = lat_deg;
   this.lat_min = lat_min;
   this.lat_sec = lat_sec;
   this.lon_deg = lon_deg;
   this.lon_min = lon_min;
   this.lon_sec = lon_sec;
   this.site_select = site_select;
}
GeolocationHelper.prototype.success = function (position)
{
   //alert("Geolocation succeded! "+position.coords.longitude);
   // update the latitude input widgets
   var lat_dms = deg2dms(position.coords.latitude);
   this.lat_deg.value = lat_dms[0].toString();
   this.lat_min.value = lat_dms[1].toString();
   this.lat_sec.value = lat_dms[2].toString();
   // update the longitude input widgets
   var lon_dms = deg2dms(position.coords.longitude);
   this.lon_deg.value = lon_dms[0].toString();
   this.lon_min.value = lon_dms[1].toString();
   this.lon_sec.value = lon_dms[2].toString();
   // reset the site selection list
   this.site_select.selectedIndex = 0;
}
GeolocationHelper.prototype.failure = function (error)
{
   if (error.code != 1)
   {
      alert("Geolocation error " + error.code
            + "\n" +  error.message);
   }
}

// This is the main object to be instantiated from
// the HTML document. It receives the handles of all the
// widgets used for configuring or displaying the datas.
function ObsEphemeris(
         div_config, show_hide_config,
         date_input,
         local_radio, tz_offs, tz_offs_dst, UTC_radio,
         lat_deg, lat_min, lat_sec,
         lon_deg, lon_min, lon_sec,
         site_select, geoloc_button,
         set_cbox, rise_cbox,
         ephem_display
      )
{
   // Config area management
   this.div_config = div_config;
   this.sh_config = 0;  // default status = hidden
   this.show_hide_config = show_hide_config;
   show_hide_config.onclick = this.toggle_config_cb.bind(this);
   // Date
   this.date_input = date_input;
   this.date = new Date();
   this.date.setUTCHours(0);
   this.date.setUTCMinutes(0);
   this.date.setUTCSeconds(0);
   // Time
   this.local_radio = local_radio;
   this.UTC_radio = UTC_radio;
   this.tz_offs = tz_offs;
   this.tz_offs_dst = tz_offs_dst;
   // Geo. coords
   this.lat_deg = lat_deg;
   this.lat_min = lat_min;
   this.lat_sec = lat_sec;
   this.lon_deg = lon_deg;
   this.lon_min = lon_min;
   this.lon_sec = lon_sec;
   // Site selection
   this.site_select = site_select;
   populate_site_select(site);
   this.site_select.onchange = this.site_change_cb.bind(this);
   // geolocation
   this.geoloc_button = geoloc_button;
   if (navigator.geolocation)
   {
      this.geoloc_button.onclick = this.geoloc_cb.bind(this);
   }
   else
   {  // not available
      this.geoloc_button.disabled = true;
   }
   // selection
   this.set_cbox = set_cbox;
   this.rise_cbox = rise_cbox;
   // display
   this.ephem_display = ephem_display;
   // compute for default values
   this.update();
}
ObsEphemeris.prototype.update = function ()
{
   // get date, as Julian Day Number
   var jd = date2jd(this.date);
   // get geographical coords
   var lat = dms2deg(parseInt(this.lat_deg.value),
                     parseInt(this.lat_min.value),
                     parseInt(this.lat_sec.value));
   var lon = dms2deg(parseInt(this.lon_deg.value),
                     parseInt(this.lon_min.value),
                     parseInt(this.lon_sec.value));
   // prepare the output table, with header
   this.ephem_display.empty();
   var disp_time_label;
   if (this.local_radio.checked)
   {
      disp_time_label = "local";
   }
   else if (this.UTC_radio.checked)
   {
      disp_time_label = "UTC";
   }
   this.ephem_display.append(
      document.documentElement.lang.substring(0,2) == "fr" ?
      "<tr><th class=\"display\">Date<br/>d'observation</th><th class=\"display\">Heure:Minute<br/>"+disp_time_label+"</th><th class=\"display\">Hauteur<br/>[deg]</th><th class=\"display\">Lever ou<br/>coucher</th></tr>" :
      "<tr><th class=\"display\">Observation<br/>date</th><th class=\"display\">Hour:Minute<br/>"+disp_time_label+"</th><th class=\"display\">Elevation<br/>[deg]</th><th class=\"display\">Rise or<br/>set</th></tr>");
   // compute the observation times for one year
   // ahead of selected date and display it if possible
   var irow = 0, last_irow = -2;
   for(var k = 0; k < 365; k++)
   {
      var obs = merc_obs(jd+k, lat, lon);
      if (obs.is_some())
      {
         var [jd_obs, merc_elev, rise_set] = obs.get_value();
         if (rise_set < 0 && this.set_cbox.checked
             || rise_set >= 0 && this.rise_cbox.checked)
         {
            // get universal time
            var ut = hr2hms(jd2ut(jd_obs));
            // create a new Date object
            var dk = new Date(this.date.valueOf() + k*86400000);
            dk.setUTCHours(ut[0]);
            dk.setUTCMinutes(ut[1]);
            dk.setUTCSeconds(0);
            // get display time
            var disp_dt;
            if (this.local_radio.checked)
            {  // apply timezone offset
               disp_dt = new Date(dk.valueOf()
                              + parseFloat(this.tz_offs.value)*3600000);
               // apply DST offset if needed
               if (is_dst(disp_dt, lat))
               {
                  disp_dt = new Date(dk.valueOf()
                              + parseFloat(this.tz_offs_dst.value)*3600000);
               }
            }
            else if (this.UTC_radio.checked)
            {
               disp_dt = dk;
            }
            // add a new row to the display table
            irow++;
            this.ephem_display.append(
                  "<tr class=\"t"
                     +(irow%2+1).toString()
                  +"\"><td class=\"display\">"
                     +disp_dt.getUTCFullYear().toString()+"/"
                     +twodig(disp_dt.getUTCMonth()+1)+"/"
                     +twodig(disp_dt.getUTCDate())
                  +"</td><td class=\"display\">"
                     +twodig(disp_dt.getUTCHours())
                     +":"+twodig(disp_dt.getUTCMinutes())
                  +"</td><td class=\"display\">"
                     +merc_elev.toFixed(1)
                  +"</td><td class=\"display\">"
                     +"<img src=\""+(rise_set < 0 ? "down.png" : "up.png")
                     +"\" alt=\""+rise_set.toString()+"\">"
                  +"</td></tr>");
            last_irow = k;
         }
      }
      else if (last_irow == k-1)
      {  // insert a blank line to separate next Mercury elongation
         this.ephem_display.append("<tr><td class=\"display\">&nbsp;</td><td class=\"display\">&nbsp;</td><td class=\"display\">&nbsp;</td><td class=\"display\">&nbsp;</td></tr>");
      }
   }
}
ObsEphemeris.prototype.toggle_config_cb = function ()
{
   // prepare the button label, according to current language
   const _SH_TEXT = 
      document.documentElement.lang.substring(0,2) == "fr" ?
      new Array("Modifier", "Appliquer") :
      new Array("Set", "Apply");
   // compute & display if the config area is displayed
   if (this.sh_config == 1)
   {
      // first, get the date from the widget
      this.date.setUTCFullYear(parseInt(this.date_input.value.substring(0, 4)));
      this.date.setUTCMonth(parseInt(this.date_input.value.substring(5, 7))-1);
      this.date.setUTCDate(parseInt(this.date_input.value.substring(8, 10)));
      this.date.setUTCHours(0);
      this.date.setUTCMinutes(0);
      this.date.setUTCSeconds(0);
      // then do the computation and update the display table
      this.update();
   }
   // toggle the hidden status
   this.div_config.toggle();
   // toggle the label
   this.sh_config = 1-this.sh_config;
   this.show_hide_config.value = _SH_TEXT[this.sh_config] + " configuration";
}
ObsEphemeris.prototype.site_change_cb = function ()
{
   var i = this.site_select[this.site_select.selectedIndex].value;
   this.lat_deg.value = SITES[i].lat_deg.toString();
   this.lat_min.value = SITES[i].lat_min.toString();
   this.lat_sec.value = SITES[i].lat_sec.toString();
   this.lon_deg.value = SITES[i].lon_deg.toString();
   this.lon_min.value = SITES[i].lon_min.toString();
   this.lon_sec.value = SITES[i].lon_sec.toString();
   this.tz_offs.value = SITES[i].tz_offs;
   this.tz_offs_dst.value = SITES[i].tz_offs_dst;
}
ObsEphemeris.prototype.geoloc_cb = function ()
{
   var geoloc = new GeolocationHelper(
                 this.lat_deg, this.lat_min, this.lat_sec,
                 this.lon_deg, this.lon_min, this.lon_sec,
                 this.site_select);
   navigator.geolocation.getCurrentPosition(
         geoloc.success.bind(geoloc),
         geoloc.failure.bind(geoloc));
}
