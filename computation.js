// -----------------------------------------------------------------------------
// File       : computation.js
// Purpose    : low precision astronomical computations
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Created on : 2021-APR-06
// Modified on: 2021-MAY-21
// Reference  : Peter Duffett-Smith, Practical Astronomy with your Calculator,
//              3rd ed., Cambridge University Press, 1995.
// -----------------------------------------------------------------------------

// -------------------------------------
//
// Miscellaneous classes
//
// -------------------------------------
// An object to represent an optional value
function SomeOrNone()
{  // no value is set by default
   this.isset = false;
}
SomeOrNone.prototype.set_value = function(x)
{  // assign a value
   this.value = x;
   this.isset = true;
}
SomeOrNone.prototype.is_some = function()
{  // tell if a value is present
   return this.isset;
}
SomeOrNone.prototype.get_value = function()
{  // call this method only if isset returned true!
   return this.value;
}
// Two factory functions: some or none
function some(x)
{  // an object with defined value
   var o = new SomeOrNone();
   o.set_value(x);
   return o;
}
function none()
{  // an empty object, with no value defined
   return new SomeOrNone();
}

// -------------------------------------
//
// Mathematical utility functions
//
// -------------------------------------

// degree to radian
function deg2rad(x)
{
   return x * Math.PI/180;
}

// radian to degree
function rad2deg(x)
{
   return x * 180 / Math.PI;
}

// Add or substract 360 to keep the argument in the range 0-360
function mod360(x)
{
   return x - 360*Math.floor(x/360);
}

// Add or substract 24 to keep the argument in the range 0-24
function mod24(x)
{
   return x - 24*Math.floor(x/24);
}

// Evaluate a polynomial:
// p[0] + p[1]*x + ... + p[p.length-1].x**(p.length-1)
function polynomial(p, x)
{
   var xa = 1;
   var r = 0;
   for(var i = 0; i < p.length; i++)
   {
      r += p[i] * xa;
      xa *= x;
   }
   return r;
}

// Write a number with at least two digits,
// putting a zero in front of single digit number
function twodig(x)
{
   if (Math.abs(x) < 10)
   {
      var s = "0" + Math.abs(x).toString();
      return x < 0 ? "-"+s : s;
   }
   else
   {
      return x.toString();
   }
}

// Convert a number from sexagesimal to string
function sexa2string(x1, x2, x3, degr=true)
{
   if (degr)
   {  // degree, arcminute, arcsecond
      return twodig(x1)+"� "+twodig(x2)+"' "+twodig(x3)+"''";
   }
   else
   {  // hour, minute, second
      return twodig(x1)+"h "+twodig(x2)+"' "+twodig(x3)+"''";
   }
}

// Product of a 3x3-matrix (defined by its columns) by a 3-elements vector
function column_matrix_dot_vector(a1, a2, a3, x)
{
   return new Array(
         a1[0]*x[0] + a2[0]*x[1] + a3[0]*x[2],
         a1[1]*x[0] + a2[1]*x[1] + a3[1]*x[2],
         a1[2]*x[0] + a2[2]*x[1] + a3[2]*x[2]
      );
}

// Difference between two 3-elements vectors
function vector_minus_vector(a, b)
{
   return new Array(
         a[0] - b[0],
         a[1] - b[1],
         a[2] - b[2]
      );
}

// Norm of a 3-elements vector
function norm_vect(a)
{
   return Math.sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}

// -------------------------------------
//
// Time and angle related functions
//
// -------------------------------------

// DELTAT = TDT - UTC
const DELTAT = 71.80  // s

// decimal hour to hour, minute, second
function hr2hms(h)
{
   var hr  = Math.floor(h)
   var min = Math.floor(60*(h - hr));
   var sec = Math.floor(60*(60*(h - hr) - min));
   return new Array(hr, min, sec);
}

// degree, minute, second to decimal degree
function dms2deg(deg, min, sec)
{
   var sign = 1;
   if (deg < 0)
   {
      sign = -1;
   }
   return sign*(Math.abs(deg) + min/60 + sec/3600);
}

// hour, minute, second to decimal hour
function hms2hr(h, m, s)
{
   // same formula as decimal degree to degree, minute, second
   return dms2deg(h, m, s);
}

// decimal degree to degree, minute, second
function deg2dms(x)
{
   var sign = 1;
   if (x < 0)
   {
      sign = -1;
   }
   var v = Math.abs(x);
   var deg = Math.floor(v);
   var min = Math.floor(60*(v - deg));
   var sec = Math.floor(60*(60*(v - deg) - min));
   return new Array(sign*deg, min, sec);
}

// Compute Julian Day Number from calendar date
//
// Arguments:
// ----------
// dt: a Date object
//
// Returns:
// --------
// the Julian Day number
function date2jd(dt)
{
   var day = dt.getUTCDate();
   var month = dt.getUTCMonth()+1;
   var year = dt.getUTCFullYear();
   var fracday = dt.getUTCHours()/24
                 + dt.getUTCMinutes()/1440
                 + dt.getUTCSeconds()/86400;

   var yprime, mprime;
   if (month == 1 || month == 2)
   {
      yprime = year-1;
      mprime = month+12;
   }
   else
   {
      yprime = year;
      mprime = month;
   }

   var A = Math.floor(yprime/100);
   var B = 2 - A + Math.floor(A/4);
   var C = Math.floor(365.25*yprime);
   var D = Math.floor(30.6001 * (mprime+1));

   return B + C + D + day + fracday + 1720994.5;
}

// Get the universal time from a Julian Day Number
//
// Arguments:
// ----------
// jd: the Julian Day Number
//
// Returns:
// --------
// the universal time, in hour
function jd2ut(jd)
{
   // floor(jd) starts at noon, so we have to change
   // the origin to midnight
   var dayfrac = jd - Math.floor(jd);
   if (dayfrac < 0.5)
   {
      dayfrac += 0.5;
   }
   else
   {
      dayfrac -= 0.5;
   }
   return dayfrac*24;
}

// number of julian centuries since J2000.0 epoch
function jd2jcent(jd)
{
   return (jd - 2451545.0) / 36525.0;
}

// Compute the Greenwich Sidereal Time from calendar date
//
// Arguments:
// ----------
// jd: the Julian Day Number
//
// Returns:
// --------
// the GST, in sidereal hours
function jd2gst(jd)
{
   // get JD at UTC midnight
   var jd0;
   var dayfrac = jd - Math.floor(jd);
   if (dayfrac < 0.5)
   {
      jd0 = Math.floor(jd)-0.5;
      dayfrac += 0.5;
   }
   else
   {
      jd0 = Math.floor(jd)+0.5;
      dayfrac -= 0.5;
   }
   var ut = dayfrac*24;

   // apply earth rotation and reduce to the range 0-24
   var T0 = mod24(polynomial(new Array(6.697374558, 2400.051336, 0.000025862),
                             jd2jcent(jd0)));

   // get gst and reduce to the range 0-24
   var gst = mod24(1.002737909*ut + T0);

   return gst;
}

// Compute the local sidereal time for a given longitude
//
// Arguments:
// ----------
// jd : the Julian Day Number
// lon: longitude, in decimal degrees, >0 for East, <0 for West
//
// Returns:
// --------
// the LST, in sidereal hours
function jd2lst(jd, lon)
{
   var t = jd2gst(jd)
   t += lon/15
   return mod24(t);
}

// Convert a Greenwich Sidereal Time into Julian Day Number
//
// Arguments:
// ----------
// gst: the Greenwich Sidereal Time, expressed in sidereal hour
// jd0: the Julian Day number at 0h
//
// Returns:
// --------
// the Julian Day Number
function gst2jd(gst, jd0)
{
   var T0 = mod24(polynomial(new Array(6.697374558, 2400.051336, 0.000025862),
                             jd2jcent(jd0)));
   return jd0 + 0.9972695663*mod24(gst-T0)/24;
}

// Convert a Local Sidereal Time into Julian Day Number
//
// Arguments:
// ----------
// gst: the Greenwich Sidereal Time, expressed in sidereal hour
// jd0: the Julian Day number at 0h
// lon: longitude, in degrees, >0 for East, <0 for West
//
// Returns:
// --------
// the Julian Day Number
function lst2jd(lst, jd0, lon)
{
   return gst2jd(lst - lon/15, jd0);
}

// Tell if daylight saving time applies to a date/time.
// The standard rule is used: in the northern hemisphere,
// DST begin at the first Sunday
// in April, and ends at the last Sunday of October.
// For the southern hemisphere, this is the opposite.
//
// Arguments:
// ----------
// dt : a Date object
// lat: the latitude, in decimal degree
//
// Returns:
// --------
// a boolean value. true if DST applies, false otherwise.
function is_dst(dt, lat)
{
   var year  = dt.getUTCFullYear();
   var month = dt.getUTCMonth()+1;
   var day   = dt.getUTCDate();
   if (month > 4 && month < 10)
   {
      return lat >= 0;
   }
   else if (month < 4 || month > 10)
   {
      return lat < 0;
   }
   else if (month == 4)
   {
      var first_sun = 0;
      // look for the first sunday in april
      for(k = 1; k <= 7; k++)
      {
         var d = new Date(Date.UTC(year, month-1, k, 0, 0, 0));
         if (d.getDay() == 0)
         {
            first_sun = k;
            break;
         }
      }
      return (lat >= 0 && day >= first_sun)
          || (lat <  0 && day <  first_sun);
   }
   else if (month == 10)
   {
      var last_sun = 31;
      // look for the last sunday in october
      for(k = 25; k <= 31; k++)
      {
         var d = new Date(Date.UTC(year, month-1, k, 0, 0, 0));
         if (d.getDay() == 0)
         {
            last_sun = k;
            break;
         }
      }
      return (lat >= 0 && day <  last_sun)
          || (lat <  0 && day >= last_sun)
   }
}


// -------------------------------------
//
// Coordinate related functions
//
// -------------------------------------

// Correct equatorial coordinates from J2000.0 epoch
// to mean coordinates for epoch at date dt
//
// Arguments:
// ----------
// dt   : a Date object
// ra0  : right ascension, in sideral hour
// dec0 : declination, in decimal degree
//
// Returns:
// --------
// a two element array containing the corrected right ascension
// in sidereal hour and the corrected declination, in decimal degree.
function correct_precession(dt, ra0, dec0)
{
   // get the coordinates, convert in radian
   var alpha = deg2rad(15*ra0);
   var delta = deg2rad(dec0);
   // get Julian date of current date/time
   var jd = date2jd(dt);
   // number of julian centuries since J2000.0 epoch
   var T = jd2jcent(jd);
   // IAU 1976 precesion angles, convert in radian
   var zeta  = deg2rad(polynomial(new Array(0, 2306.2181, 0.30188, 0.017998), T)/3600);
   var z     = deg2rad(polynomial(new Array(0, 2306.2181, 1.09468, 0.018203), T)/3600);
   var theta = deg2rad(polynomial(new Array(0, 2004.3109, 0.42665, 0.041833), T)/3600);
   // pre-compute some trigonometry functions
   var cd0 = Math.cos(delta);
   var sd0 = Math.sin(delta);
   var cth = Math.cos(theta);
   var sth = Math.sin(theta);
   var caz = Math.cos(alpha+zeta);
   var saz = Math.sin(alpha+zeta);
   // compute corrected coordinates
   var ra1 = rad2deg(z+Math.atan2(cd0*saz, cth*cd0*caz-sth*sd0))/15;
   ra1 -= 24*Math.floor(ra1/24);
   var dec1 = rad2deg(Math.asin(sth*cd0*caz+cth*sd0));
   return new Array(ra1, dec1);
}

// Convert equatorial coordinates into local horizontal coordinates
//
// Arguments:
// ----------
// ra : right ascension, in sideral hour
// dec: declination, in degree
// jd : a Julian Day Number
// lat: latitude, in degree
// lon: longitude, in degree
//
// Returns:
// ----------
// a two-element array containing the azimuth and elevation,
// expressed in decimal degree
function equatorial2local(
      ra, dec,
      jd,
      lat, lon)
{
   // hour angle, in degree
   var H = 15 * (jd2lst(jd, lon) - ra);
   // correct for the difference between TDT and UTC
   //H += 15 * DELTAT/3600 * 2400.051336/36525.0;
   // set into the range 0-360
   H = mod360(H);

   // pre-compute all the trigonometric functions
   var cd   = Math.cos(deg2rad(dec));
   var sd   = Math.sin(deg2rad(dec));
   var cphi = Math.cos(deg2rad(lat));
   var sphi = Math.sin(deg2rad(lat));
   var cH   = Math.cos(deg2rad(H));
   var sH   = Math.sin(deg2rad(H));

   // get the two angles, in decimal degrees
   var elev = rad2deg(Math.asin(sd*sphi+cd*cphi*cH));
   var sel  = Math.sin(deg2rad(elev));
   var az   = rad2deg(Math.atan2(-cd*cphi*sH, sd-sphi*sel));
   az = mod360(az);

   return new Array(az, elev);
}

// Return the mean obliquity of the ecliptic.
// Low precision, but good enough for most uses. [Meeus-1998: equation 22.2].
// Accuracy is 1" over 2000 years and 10" over 4000 years.
function obliquity(jd)
{
   const _el0 = new Array(
      dms2deg(23, 26, 21.448),
      dms2deg(0, 0, -46.8150),
      dms2deg(0, 0, -0.00059),
      dms2deg(0, 0, 0.001813));
   var T = jd2jcent(jd);
   return polynomial(_el0, T);
}

// Convert from ecliptic coordinates to equatorial coordinates
//
// Arguments:
// ----------
// jd : the Julian day number, used for computing the obliquity
// lat: the ecliptic latitude, in decimal degree
// lon: the ecliptic longitude, in decimal degree
//
// Returns:
// --------
// A two-element array containing the right ascension in sidereal hour
// and declination angle, expressed in decimal degree
function ecliptic2equatorial(jd, lat, lon)
{
   var obli = obliquity(jd);

   // pre-compute some trigonometric factors
   var cose = Math.cos(deg2rad(obli));
   var sine = Math.sin(deg2rad(obli));
   var coslo = Math.cos(deg2rad(lon));
   var sinlo = Math.sin(deg2rad(lon));
   var cosla = Math.cos(deg2rad(lat));
   var sinla = Math.sin(deg2rad(lat));

   // compute right ascension and declination angles, in decimal degree
   var ra = mod24(rad2deg(
      Math.atan2(sinlo * cose - sinla / cosla * sine, coslo))/15);
   var dec = rad2deg(Math.asin(sinla * cose + cosla * sine * sinlo));
   return new Array(ra, dec);
}

// Solve the Kepler equation
//
// Arguments:
// ----------
// M  : the mean anomaly, expressed in radian
// ecc: the eccentricity
//
// Returns:
// --------
// the eccentric anomaly, expressed in radian
function solve_kepler_equation(M, ecc)
{
   var E = M;
   var delta = E - ecc*Math.sin(E) - M;
   while(Math.abs(delta) > 1e-6)
   {
      E -= delta/(1-ecc*Math.cos(E));
      delta = E - ecc*Math.sin(E) - M;
   }
   return E;
}

// -------------------------------------
//
// Ephemeris
//
// -------------------------------------
// Low precision ephemeris for the Sun. Accurate to 0.01 degree.
// The latitude is presumed to be zero.
function sun_position(jd)
{
   const _kL0 = new Array(280.46646, 36000.76983, 0.0003032);
   const _kM = new Array(357.5291092, 35999.0502909, -0.0001536, 1.0/24490000);
   const _kC = new Array(1.914602, -0.004817, -0.000014);
   const _ck3 = 0.019993;
   const _ck4 = -0.000101;
   const _ck5 = 0.000289;

   // compute ecliptic longitude, expressed in degree
   var T = jd2jcent(jd);
   var L0 = polynomial(_kL0, T);
   var M = deg2rad(polynomial(_kM, T));
   var C = polynomial(_kC, T) * Math.sin(M)
        + (_ck3 - _ck4 * T) * Math.sin(2 * M)
        + _ck5 * Math.sin(3 * M);
   var L = L0+C;
   L = mod360(L);

   return ecliptic2equatorial(jd, 0, L);
}
// Compute the orbital position of an object in rectangular coordinates,
// usually the heliocentric ecliptic rectangular coordinates of the planet
// which orbital elements are given as argument, for Julian day number jd
//
// Arguments:
// ----------
// jd        : the Julian day number at which the position is desired
// jd_epoch  : the Julian day number of the epoch
//             for the following orbital elements
// sid_period: the sidereal period, expressed in day
// mean_lon  : the mean longitude at epoch, expressed in degree
// peri_lon  : the longitude of periastron, expressed in degree
// ecc       : the eccentricity
// sma       : the semi major axis, in arbitrary unit
// incli     : the inclination, expressed in degree
// ascn_lon  : the longitude of the ascending node, expressed in degree
//
// Returns:
// --------
// a 3-elements vector, the rectangular coordinates of object,
// expressed in the same unit as sma
function orbital_position(jd, jd_epoch, sid_period,
                          mean_lon, peri_lon, ecc, sma, incli, ascn_lon)
{
   // mean anomaly, expressed in radian
   var mean_anom = 2*Math.PI*(jd-jd_epoch)/sid_period
                   + deg2rad(mean_lon - peri_lon);
   // eccentric anomaly, expressed in radian
   var ecc_anom = solve_kepler_equation(mean_anom, ecc);
   // heliocentric orbital plane rectangular coordinates,
   // expressed in the same unit as sma
   var p0 = new Array(
         sma*(Math.cos(ecc_anom)-ecc),
         sma*Math.sqrt(1-ecc*ecc)*Math.sin(ecc_anom),    
         0
      );
   // argument of perihelion, expressed in radian
   var peri_arg = deg2rad(peri_lon - ascn_lon);
   var cpa = Math.cos(peri_arg);
   var spa = Math.sin(peri_arg);
   // coordinate system change from orbital plane to heliocentric ecliptic
   var p1 = column_matrix_dot_vector(
         new Array(  cpa, spa, 0 ),
         new Array( -spa, cpa, 0 ),
         new Array(    0,   0, 1 ),
         p0
      );
   var ci = Math.cos(deg2rad(incli));
   var si = Math.sin(deg2rad(incli));
   var p2 = column_matrix_dot_vector(
         new Array( 1,   0,  0 ),
         new Array( 0,  ci, si ),
         new Array( 0, -si, ci ),
         p1
      );
   var ca = Math.cos(deg2rad(ascn_lon));
   var sa = Math.sin(deg2rad(ascn_lon));
   var p3 = column_matrix_dot_vector(
         new Array(  ca, sa, 0 ),
         new Array( -sa, ca, 0 ),
         new Array(   0,  0, 1 ),
         p2
      );
   // return the rectangular coordinates as 3-elements vector,
   // expressed in the unit of sma
   return p3;
}
function planet_position(jd, epoch, sid_period,
                         mean_lon, peri_lon, ecc, sma, incli, ascn_lon)
{
   // Earth year definitions, expressed in days
   const tropical_year = 365.242191;
   const sidereal_year = 365.2564;    
   // get Julian day number of epoch (= first day of year)
   var d0 = new Date();
   d0.setUTCFullYear(epoch); d0.setUTCMonth(0); d0.setUTCDate(0);
   d0.setUTCHours(0); d0.setUTCMinutes(0); d0.setUTCSeconds(0);
   var jd_epoch = date2jd(d0);
   // geocentric ecliptic rectangular coordinates
   var gerc = vector_minus_vector(
      orbital_position(jd, jd_epoch, sid_period*tropical_year, mean_lon, peri_lon, ecc, sma, incli, ascn_lon),
      orbital_position(jd, jd_epoch, sidereal_year, 99.403308, 102.768413, 0.016713, 1, 0, 0) // earth
   );
   // geocentric ecliptic spherical coordinates
   var ecl_lat = rad2deg(Math.asin(gerc[2]/norm_vect(gerc)));
   var ecl_lon = rad2deg(Math.atan2(gerc[1], gerc[0]));
   return ecliptic2equatorial(jd, ecl_lat, ecl_lon);
}
function mercury_position(jd)
{
   return planet_position(jd, 1990, 0.240852, 60.750646, 77.299833, 0.205633, 0.387099, 7.004540, 48.212740);
}

// Given a target elevation, a declination and a latitude,
// compute the hour angles for which the object elevation
// is equal to target.
//
// Arguments:
// ----------
// target_elev: the desired elevation, in degree
// dec        : the object declination, in sidereal hour
// phi        : the observer latitude, in degree
//
// Returns:
// --------
// An array containing zero or two hour angles (expressed in
// sidereal hour), one at setting, one at rising.
function alt2HA(target_elev, dec, phi)
{
   var HA = new Array();

   var cp = Math.cos(deg2rad(phi));
   var sp = Math.sin(deg2rad(phi));
   var cd = Math.cos(deg2rad(dec));
   var sd = Math.sin(deg2rad(dec));
   var sa = Math.sin(deg2rad(target_elev));
   var y = (sa-sp*sd)/(cp*cd);
   if (Math.abs(y) <= 1)
   {
      var x1 = rad2deg(Math.acos(y));
      var x2 = 360-x1;
      HA = new Array(x1/15, x2/15);
   }
   return HA;
}

// Get the time for observing Mercury, given a date and a site
//
// Arguments:
// ----------
// jd0: the Julian Day Number at midnight of the desired day 
// lat: the latitude, in degrees
// lon: the longitude, in degrees, >0 east, <0 west
//
// Returns:
// --------
// A SomeOrNone object.
// If Mercury can be observed, it carries an array with three values.
// First value is the Julian Day Number at observation time.
// Second value if the elevation of Mercury at obs time, in degree.
// Third value is a flag +1 for sunrise, -1 for sunset
function merc_obs(jd0, lat, lon)
{
   const _SUN_H  = -6.0;  // deg., end of civil twilight, begin of civil dawn
   const _MERC_H =  7.0;  // deg., minimum elevation of Mercury

   // get sun coords at jd0
   var sun0 = sun_position(jd0);

   // get the hour angle of then sun when its elevation is _SUN_H
   var ha0 = alt2HA(_SUN_H, sun0[1], lat);
   if (ha0.length == 0)
      return none();

   // iterate one more time to have better accuracy
   var jd = new Array();
   var sun_rise = new Array();
   for(var k1 = 0; k1 < ha0.length; k1++)
   {
      // get the JDN for ha0
      jd[k1] = lst2jd(mod24(sun0[0]+ha0[k1]), jd0, lon);
      sun_rise[k1] = ha0[k1] < 12 ? -1 : 1;
      // recompute sun position, to get more precise coords
      var sun1 = sun_position(jd[k1]);
      // recompute the hour angle of the sun for elevation _SUN_H
      var ha1 = alt2HA(_SUN_H, sun1[1], lat);
      if (ha1.length > 0)
      {
         // look for the closest value of the time
         var djdmin = 365;
         var jdmin  = -1;
         var kmin   = -1;
         for (var k2 = 0; k2 < ha1.length; k2++)
         {
            // get the JDN for ha1
            var jd2 = lst2jd(mod24(sun1[0]+ha1[k2]), jd0, lon);
            if (Math.abs(jd[k1] - jd2) < djdmin)
            {
               djdmin = Math.abs(jd[k1] - jd2);
               jdmin  = jd2;
               kmin   = k2;
            }
         }
         if (jdmin != -1)
         {
            jd[k1] = jdmin;  // update with more precise value of the JDN
            sun_rise[k1] = ha1[kmin] < 12 ? -1 : 1;
         }
         else
         {
            alert("jdmin=-1!");
         }
      }
   }

   // now, get the elevation of Mercury
   for(var k = 0; k < jd.length; k++)
   {
      var merc_position = mercury_position(jd[k]);
      var merc_azel = equatorial2local(merc_position[0], merc_position[1],
                                       jd[k], lat, lon);
      // return immediatly if elevation above threshold
      if (merc_azel[1] >= _MERC_H)
      {
         return some(new Array(jd[k], merc_azel[1], sun_rise[k]));
      }
   }
   return none();
}
