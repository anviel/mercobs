## MercObs: Mercury Observation Ephemeris
*MercObs* is a web application that computes and displays the date and time at which the Mercury planet can be observed, over one year from a starting date.

## Dependencies
* [jquery](https://github.com/jquery/jquery)

## Principle
For each day of the year from a starting date, the application computes the time of the end of civil twilight (when the sun elevation is exactly 6 degrees below the horizon), and the time of the beginning of civil dawn. If the elevation of Mercury at one of these two dates is greater than 7 degrees, observation conditions are reported for this date.
Observation conditions are reported as:
* the date;
* the time of civil twilight (if sunset) or civil dawn (if sunrise), displayed as local or UTC time;
* the elevation of Mercury at this time, expressed in degree;
* a symbol that tells if it is sunset or sunrise.

## Getting started
Open the *mercobs_en.html*, or the *mercobs_fr.html* for the french version.
Click the *Set configuration* button to change the starting date, observation site, or observation conditions.

## Usage
By default, the starting date is the current date, and a default observation site is used. By clicking the button titled *Set configuration*, the configuration area is shown. From there, it is possible to
* select a new date as a starting point for computation;
* switch between local or UTC time for display;
* enter the geographical coordinates of the observation site;
* or, select an observation site from a list;
* or, click the *Geolocation* button to use the geolocation facility of the web browser (usually a dialog box asks permission about it);
* select only the observing conditions for sunset and/or sunrise.

To update the results using the new configuration, click the *Apply configuration* button.

